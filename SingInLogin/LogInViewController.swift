//
//  LogInViewController.swift
//  SingInLogin
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    static var personList: [String : String] = ["Sida": "55555" ]
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet weak var textFieldUserName: UITextField!
    @IBOutlet weak var labelUserName: UILabel!
    
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var labelUserStatus: UILabel!
    
    var name: String?
    var password: String?
    var delegateLogin : LogInDelegate?
    
    
    @IBAction func LogInTep(_ sender: Any) {
        name = textFieldUserName.text
        password = textFieldPassword.text
        if LogInViewController.personList.isEmpty{
            labelUserStatus.text = "Hello ! You don't have account yes."
        }
        if name == "" {
            labelUserName.text = "Please Input User Name!"
        }
        else {
            var conditionName = 0 , conditionPassword = 0;
            for (key, value) in LogInViewController.personList{
                if key == name && value == password{
                        delegateLogin?.didLogIn(userName: key, buttonLabel: "LOG OUT")
                        navigationController?.popViewController(animated: true)
                }
                else if key != name{
                    conditionName += 1
                }
                else if value  != password{
                    conditionPassword += 1
                }
            }
            if conditionName == LogInViewController.personList.count{
                labelPassword.text = ""
                labelUserName.text = "Incorrect user name"
            }
            if conditionPassword == LogInViewController.personList.count{
                labelUserName.text = ""
                labelPassword.text = "Incorrect Password"
            }
        }
        
    }
    
    @IBAction func SignUpTep(_ sender: Any) {
        guard  let signUp = self.storyboard?.instantiateViewController(withIdentifier: "signUpform") as? SignUpViewController else {
            fatalError("View Controller not found")
        }
         signUp.delegate = self
        navigationController?.pushViewController(signUp, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        btnLogin.layer.cornerRadius = 8.0
        btnLogin.layer.masksToBounds = true
        btnSignUp.layer.cornerRadius = 8.0
        btnSignUp.layer.masksToBounds = true
    }
    

}

extension LogInViewController: SignUpDelegate{
    func didSignIn(userName: String, password: String) {
        LogInViewController.personList[userName] = password
    }
}
