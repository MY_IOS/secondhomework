//
//  SignUpDelegate.swift
//  SignUp
//
//  Created by Apple on 11/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol SignUpDelegate {
    func didSignIn(userName : String,  password : String) 
}

protocol LogInDelegate {
    func didLogIn(userName : String, buttonLabel : String) 
}
