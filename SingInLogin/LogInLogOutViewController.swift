//
//  LogInLogOutViewController.swift
//  SingInLogin
//
//  Created by Apple on 11/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LogInLogOutViewController: UIViewController {
    @IBOutlet weak var buttonLogOut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonLogOut.layer.cornerRadius = 8.0
        buttonLogOut.layer.masksToBounds = true
        
    }
    
    @IBAction func LogInTap(_ sender: Any) {
        guard  let logInform = self.storyboard?.instantiateViewController(withIdentifier: "logInform") as? LogInViewController else {
            fatalError("View Controller not found")
        }
//        logInform.delegate = self
        navigationController?.pushViewController(logInform, animated: true)
    }
}
