//
//  SignUpViewController.swift
//  SingInLogin
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet weak var textFieldUserName: UITextField!
    @IBOutlet weak var labelUserName: UILabel!
    
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var labelPassword: UILabel!
    
    @IBOutlet weak var textFieldRePassword: UITextField!
    @IBOutlet weak var labelRePassword: UILabel!
    
    var delegate : SignUpDelegate?
    var name: String?
    var password: String?
    var re_password: String?
    
    @IBAction func signUpTep(_ sender: Any) {
       name = textFieldUserName.text
        password = textFieldPassword.text
        re_password = textFieldRePassword.text
        if name! == "" {
            labelUserName.text = "Please Input Name!"
            labelRePassword.text = ""
        }
        else if password! != re_password! {
            labelRePassword.text = "Password Not Match!"
            labelUserName.text = ""
        }
        else{
            delegate?.didSignIn(userName: name!, password: password!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSignUp.layer.cornerRadius = 8.0
        btnSignUp.layer.masksToBounds = true
    }
    


}


