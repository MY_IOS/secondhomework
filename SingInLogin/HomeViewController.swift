//
//  HomeViewController.swift
//  Home
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

   
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var btnLogIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogIn.layer.cornerRadius = 8.0
        btnLogIn.layer.masksToBounds = true
    }
    
    @IBAction func LogInTap(_ sender: Any) {
            guard  let logIn = self.storyboard?.instantiateViewController(withIdentifier: "logInform") as? LogInViewController else {
                fatalError("View Controller not found")
            }
            logIn.delegateLogin = self
            navigationController?.pushViewController(logIn, animated: true)
    }
}

extension HomeViewController: LogInDelegate{
    func didLogIn(userName: String, buttonLabel: String) {
        labelStatus.text = "Welcome \(userName)"
        btnLogIn.setTitle(buttonLabel, for: .normal)
    }
}
